program main
use integral
implicit none
real(8) h, a, b, eps, c, a1, b1, h1
integer n, i, n1

open(1, file = "input") !обычный интеграл
open(2, file = "input1") !интеграл с особенностью
open(3, file = "result")

read (1,*) a, b, n  !для обычного интеграла
read (2,*) a1, b1, n1, c, eps ! для интеграла с особенностью


write(3,*) "значение интеграла от f(x) = x", &
	    gauss(f, a, b, n)
	
write(3,*) "значение интеграла от f(x) = sinx/x на промежутке от -pi/4 до pi/4", &
	    gauss_improper(f1, a1, b1, eps, c, n1)

end program main
