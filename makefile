com=gfortran
ras=f95
files=$(wildcard *.$(ras))
main : $(patsubst %.$(ras), %.o, $(files))
	 $(com) $^ -o $@
%.o : %.$(ras) integral.mod
	 $(com) -c $<
integral.mod : integral.f95
	 $(com) -c $<
result : main input
	 ./main < input > result
clean :
	 rm -f *.o; rm -f main
