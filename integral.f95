module integral
implicit none

contains

   function f(x); real(8) f,x; f=x; end function f

   function f1(x); real(8) f1,x; f1=sin(x)/x; end function f1

   
	function gauss(f, a, b, n); real(8) gauss, a, b, h; real(8), allocatable:: x(:), s(:); real(8), external :: f; integer i,n 

	h = (b - a) / n
	allocate(x(1:n), s(1:n))

	do i=1, n
        	x(i) = a+(i-1)*h
	enddo

	do i=1, n-1
 	 s(i) = ( f( (x(i)+x(i+1))/2 - sqrt(3.0_8)/3 *(x(i+1)-x(i))/2) + f((x(i)+x(i+1))/2 + sqrt(3.0_8)/3 *(x(i+1)-x(i))/2) ) &
	 * (x(i+1) - x(i))/2 
	enddo

	gauss = sum(s)

    end function gauss


   function gauss_improper(f1, a, b, eps, c, n); real(8) a, b, c, eps, I1, I2, h, gauss_improper; integer n,i; 
						 real(8), external :: f1

	if (  gauss(f1, c - 0.001, c + 0.001, n) < eps/2) then
		
		I1 = gauss(f1, a, c - 0.001, n)  
		I2 = gauss(f1, c + 0.001, b, n)
		
	endif	
	gauss_improper = I1 + I2

   end function gauss_improper



end module integral
